// -*- coding: utf-8 -*-

'use strict';

(function () {
    var requesting = false;
    var test_request = function (url, use_custom_header) {
        if (requesting) {
            setTimeout(function () { test_request(url, use_custom_header); }, 1000);
            return;
        }
        requesting = true;
        var message = [
            '--- testing request ---',
            '    to:                ' +  url,
            '    use custom header: ' +  use_custom_header
        ].join('\n');
        console.log(message);
        var req = new XMLHttpRequest();
        req.open('GET', url, true);
        if (use_custom_header) {
            req.setRequestHeader('X-Prevent-Redirect', 'true');
        }
        req.addEventListener('load', function (e) {
            console.log('succeeded!');
            requesting = false;
        }, false);
        req.addEventListener('error', function (e) {
            console.log('falied!');
            requesting = false;
        }, false);
        req.send(null);
    };
    var redirect_url = document.querySelector('a[rel="next"]').href;
    var direct_url = decodeURIComponent(redirect_url.split(/\?url=/)[1]);
    var same_origin_url = location.href;
    test_request(redirect_url, true);
    test_request(redirect_url, false);
    test_request(direct_url, true);
    test_request(direct_url, false);
    test_request(same_origin_url, true);
    test_request(same_origin_url, false);
}());

/*
 * Environment:
 *   UA: Mozilla/5.0 (X11; Linux i686; rv:11.0a2) Gecko/20120131 Firefox/11.0a2
 *   Add-on SDK: 1.5b3
 * Result (in Web Console):
 *     --- testing request ---
 *     to:                http://misc-xkyrgyzstan.dotcloud.com/aptests/open_redirector?url=http%3A%2F%2Fpure-stream-9966.herokuapp.com%2Fredirect%2Fng%2Fcors
 *     use custom header: true
 *     falied!
 *     --- testing request ---
 *     to:                http://misc-xkyrgyzstan.dotcloud.com/aptests/open_redirector?url=http%3A%2F%2Fpure-stream-9966.herokuapp.com%2Fredirect%2Fng%2Fcors
 *     use custom header: false
 *     succeeded!
 *     --- testing request ---
 *     to:                http://pure-stream-9966.herokuapp.com/redirect/ng/cors
 *     use custom header: true
 *     succeeded!
 *     --- testing request ---
 *     to:                http://pure-stream-9966.herokuapp.com/redirect/ng/cors
 *     use custom header: false
 *     succeeded!
 *     --- testing request ---
 *     to:                http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors
 *     use custom header: true
 *     succeeded!
 *     --- testing request ---
 *     to:                http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors
 *     use custom header: false
 *     succeeded!
 *
 * Result (in Add-on):
 *     --- testing request ---
 *     to:                http://misc-xkyrgyzstan.dotcloud.com/aptests/open_redirector?url=http%3A%2F%2Fpure-stream-9966.herokuapp.com%2Fredirect%2Fng%2Fcors
 *     use custom header: true
 *     falied!
 *     --- testing request ---
 *     to:                http://misc-xkyrgyzstan.dotcloud.com/aptests/open_redirector?url=http%3A%2F%2Fpure-stream-9966.herokuapp.com%2Fredirect%2Fng%2Fcors
 *     use custom header: false
 *     succeeded!
 *     --- testing request ---
 *     to:                http://pure-stream-9966.herokuapp.com/redirect/ng/cors
 *     use custom header: true
 *     succeeded!
 *     --- testing request ---
 *     to:                http://pure-stream-9966.herokuapp.com/redirect/ng/cors
 *     use custom header: false
 *     succeeded!
 *     --- testing request ---
 *     to:                http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors
 *     use custom header: true
 *     succeeded!
 *     --- testing request ---
 *     to:                http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors
 *     use custom header: false
 *     succeeded!
 */