// -*- coding: utf-8 -*-

'use strict';

var pageMod = require('page-mod');
var self = require('self');

exports.main = function (options, callbacks) {
    pageMod.PageMod({
        include: ['http://misc-xkyrgyzstan.dotcloud.com/aptests/redirect/ng/cors'],
        contentScriptWhen: 'ready',
        contentScriptFile: [self.data.url('prohibit_redirect.js')]
    });
};
